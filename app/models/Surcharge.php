<?php

class Surcharge extends Eloquent
{
	/**
     * Model 'Surcharge' table
     * @var string
     */
	protected $table = 'surcharge';
	protected $primaryKey = 'surcharge_id';
        
        public $timestamps = false;
}